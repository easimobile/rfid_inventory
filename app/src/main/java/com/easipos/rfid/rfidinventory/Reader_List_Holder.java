package com.easipos.rfid.rfidinventory;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class Reader_List_Holder extends RecyclerView.ViewHolder {

    TextView id;
    TextView mac;

    Reader_List_Holder(View itemView) {
        super(itemView);
        id = (TextView) itemView.findViewById(R.id.reader_id);
        mac = (TextView) itemView.findViewById(R.id.reader_mac);
    }

}
