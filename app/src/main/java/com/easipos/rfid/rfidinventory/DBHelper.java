package com.easipos.rfid.rfidinventory;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;


public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "EASIHandheld";
    public static final String RFID_TABLE_NAME = "RFID";
    public static final String RFID_COLUMN_ID = "UniqueID";
    public static final String RFID_COLUMN_CODE = "ProductCode";
    public static final String RFID_COLUMN_NAME = "ProductName";
    public static final String RFID_COLUMN_FOUND = "Found";

    public static final String BALANCE_TABLE_NAME = "BALANCE";
    public static final String BALANCE_COLUMN_CODE = "ProductCode";
    public static final String BALANCE_COLUMN_COUNT = "Count";

    private Context baseContext;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME , null, 5);
        baseContext = context;

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL(
                "create table RFID " +
                        "(UniqueID text primary key, ProductCode text,ProductName text, Found integer)"
        );

        db.execSQL(
                "create table BALANCE " +
                        "(ProductCode text primary key, Count integer)"
        );

        db.execSQL(
                "create table RFID_Kill_Master " +
                        "(UniqueID text primary key, ProductCode text,ProductName text)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS RFID");
        db.execSQL("DROP TABLE IF EXISTS BALANCE");
        db.execSQL("DROP TABLE IF EXISTS RFID_Kill_Master");
        onCreate(db);
    }

    public boolean insertDataIntoRFID (ArrayList<RFIDEntryData> data) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues contentValues = new ContentValues();
            for(int i = 0 ; i <data.size() ; i++) {
                contentValues.put("UniqueID", data.get(i).getID());
                contentValues.put("ProductCode", data.get(i).getCode());
                contentValues.put("ProductName", data.get(i).getName());
                contentValues.put("Found", 0);
                db.insert("RFID", null, contentValues);
            }
            db.setTransactionSuccessful();
        }finally
        {
            db.endTransaction();
        }
        return true;
    }

    public boolean insertDataIntoRFIDKillMaster (List<RFIDEntryData> data) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues contentValues = new ContentValues();
            for (int i = 0; i < data.size(); i++) {
                contentValues.put("UniqueID", data.get(i).getID());
                contentValues.put("ProductCode", data.get(i).getCode());
                contentValues.put("ProductName", data.get(i).getName());
                db.insert("RFID_Kill_Master", null, contentValues);
            }
            db.setTransactionSuccessful();
        }finally
        {
            db.endTransaction();
        }
        return true;
    }

    public RFIDEntryData getTagInfoFromRFIDKillMaster(String tid)
    {
        String query = "select * from RFID_Kill_Master where UniqueID = '"+ tid + "'";
        RFIDEntryData result;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( query, null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            result = new RFIDEntryData(res.getString(res.getColumnIndex(RFID_COLUMN_ID)),res.getString(res.getColumnIndex(RFID_COLUMN_CODE)),res.getString(res.getColumnIndex(RFID_COLUMN_NAME)));
            return result;
        }
        return null;
    }

    public int numberOfRows(String table){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, table);
        return numRows;
    }

    public FoundAndNotFoundRecordNumbers getNumberOfFoundAndNotFound(){

        String foundQuery = "SELECT  * FROM RFID WHERE Found = 1";
        String notFoundQuery = "SELECT  * FROM RFID WHERE Found = 0";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(foundQuery, null);
        int foundCount = cursor.getCount();

        cursor = db.rawQuery(notFoundQuery, null);
        int notFoundCount = cursor.getCount();

        FoundAndNotFoundRecordNumbers result = new FoundAndNotFoundRecordNumbers(foundCount,notFoundCount);
        cursor.close();

        return result;
    }

    public boolean updateStatusToFound(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("Found", 1);
        db.update("RFID", contentValues, "UniqueID = ? ", new String[] { id } );
        return true;
    }

     public Integer deleteAllData(String table) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(table,null,null);
    }

    public ArrayList<RFIDEntryData> getNotFoundRFIDTag() {
        ArrayList<RFIDEntryData> array_list = new ArrayList<>();

        //hp = new HashMap();
        RFIDEntryData result;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from RFID where Found = 0", null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            result = new RFIDEntryData(res.getString(res.getColumnIndex(RFID_COLUMN_ID)),res.getString(res.getColumnIndex(RFID_COLUMN_CODE)),res.getString(res.getColumnIndex(RFID_COLUMN_NAME)));
            array_list.add(result);
            res.moveToNext();
        }
        return array_list;
    }

    public SOAPWeb.SOAPReturn processStockCount()
    {
        List<StockCountResultItem> stockCountResult = new ArrayList<>();
        List<RFIDEntryData> rfidEntryData = new ArrayList<>();
        List<MissingRFIDItem> missingRFIDResult = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery( "select ProductCode,count(*) AS balance from RFID where Found = 1 group by ProductCode", null );

        res.moveToFirst();

        while(res.isAfterLast() == false){
            stockCountResult.add(new StockCountResultItem(res.getString(res.getColumnIndex("ProductCode")),res.getInt(res.getColumnIndex("balance"))));
            res.moveToNext();
        }

        rfidEntryData = getNotFoundRFIDTag();

        for( int i = 0 ; i<rfidEntryData.size() ; i ++)
        {
            missingRFIDResult.add(new MissingRFIDItem(rfidEntryData.get(i).getID(),rfidEntryData.get(i).getCode()));
        }

        SOAPWeb updateStock = new SOAPWeb(baseContext);

        return updateStock.updateStockCheckResultToWeb(stockCountResult,missingRFIDResult);
    }

    public class FoundAndNotFoundRecordNumbers{
        private int Found;
        private int NotFound;

        FoundAndNotFoundRecordNumbers(int Found,int NotFound)
        {
            this.Found = Found;
            this.NotFound = NotFound;
        }

        public int getFound(){
            return Found;
        }

        public int getNotFound(){
            return NotFound;
        }
    }
}