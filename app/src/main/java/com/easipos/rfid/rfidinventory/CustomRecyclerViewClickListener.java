package com.easipos.rfid.rfidinventory;

import android.app.Fragment;
import android.view.View;

public interface CustomRecyclerViewClickListener {
    public void onItemClick(View v, int position);
}
