package com.easipos.rfid.rfidinventory;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

public class KillRFIDTagViewHolder extends RecyclerView.ViewHolder{

    TextView tid;
    TextView productCode;
    TextView productName;
    CheckBox checkBox;

    KillRFIDTagViewHolder(View itemView) {
        super(itemView);
        tid = (TextView) itemView.findViewById(R.id.kill_RFID_tid);
        productCode = (TextView) itemView.findViewById(R.id.kill_RFID_product_code);
        productName = (TextView) itemView.findViewById(R.id.kill_RFID_product_name);
        checkBox = (CheckBox) itemView.findViewById(R.id.kill_RFID_checkBox);

    }

    public void setCheckBox(boolean check)
    {
        checkBox.setChecked(check);
    }

}
