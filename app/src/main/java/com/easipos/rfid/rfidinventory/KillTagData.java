package com.easipos.rfid.rfidinventory;

public class KillTagData {

    private String tid;
    private String productCode;
    private String productName;
    private boolean checked;

    KillTagData(String title, String description, String productName) {
        this.tid = title;
        this.productCode = description;
        this.productName = productName;
        this.checked = false;
    }

    public String getID(){ return this.tid;}
    public String getCode(){ return this.productCode;}
    public String getName(){ return this.productName;}
    public boolean getChecked(){ return this.checked; }

    public void setChecked(boolean check){ this.checked = check;}

}
