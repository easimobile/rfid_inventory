package com.easipos.rfid.rfidinventory;

public class RFIDEntryData {
    private String tid;
    private String productCode;
    private String productName;

    RFIDEntryData(String title, String description, String productName) {
        this.tid = title;
        this.productCode = description;
        this.productName = productName;
    }

    public String getID(){ return this.tid;}
    public String getCode(){ return this.productCode;}
    public String getName(){ return this.productName;}
}