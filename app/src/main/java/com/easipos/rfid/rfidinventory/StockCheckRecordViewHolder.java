package com.easipos.rfid.rfidinventory;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class StockCheckRecordViewHolder extends RecyclerView.ViewHolder {

    TextView refNo;

    StockCheckRecordViewHolder(View itemView) {
        super(itemView);
        this.refNo = (TextView) itemView.findViewById(R.id.refNo);
    }
}