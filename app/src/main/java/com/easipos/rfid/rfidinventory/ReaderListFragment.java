package com.easipos.rfid.rfidinventory;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.bluetooth.BluetoothAdapter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.zebra.rfid.api3.InvalidUsageException;
import com.zebra.rfid.api3.OperationFailureException;
import com.zebra.rfid.api3.RFIDResults;
import com.zebra.rfid.api3.ReaderDevice;
import com.zebra.rfid.api3.Readers;
import com.zebra.rfid.api3.RegulatoryConfig;

import java.util.ArrayList;
import java.util.List;

public class ReaderListFragment extends Fragment {

    private static List<ReaderDevice> readerList = new ArrayList<>();
    private Readers readers;
    private RFIDReaderAdapter readerAdapter;
    private RecyclerView readerListView;
    private DeviceConnectTask deviceConnectTask;
    private CustomProgressDialog progressDialog;
    public String callerActivity;

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_readers_list, container, false);

        readerListView = view.findViewById(R.id.reader_list_view);

        readers = new Readers();

        readerList = new ArrayList<>();
        readerList.clear();
        readerList.addAll(readers.GetAvailableRFIDReaderList());


        readerAdapter = new RFIDReaderAdapter(readerList,new CustomRecyclerViewClickListener() {

            @Override
            public void onItemClick(View v, int position) {
                // do what ever you want to do with it
                if (BluetoothAdapter.getDefaultAdapter().isEnabled()) {
                    ReaderDevice readerDevice = readerList.get(position);
                    if (Global.connectedReader == null) {
                        if (deviceConnectTask == null || deviceConnectTask.isCancelled()) {
                            deviceConnectTask = new DeviceConnectTask(readerDevice, "Connecting with " + readerDevice.getName(), null,Global.readerListFragment);
                            deviceConnectTask.execute();
                        }
                    } else
                    {
                        try {
                            Global.connectedReader.disconnect();
                        }catch (InvalidUsageException e) {
                            e.printStackTrace();
                        } catch (OperationFailureException e) {
                            e.printStackTrace();
                        }
                        deviceConnectTask = new DeviceConnectTask(readerDevice, "Connecting with " + readerDevice.getName(), null,Global.readerListFragment);
                        deviceConnectTask.execute();
                    }
                }
                else
                {
                    {
                        Toast.makeText(getActivity(), "Please enable the bluetooth.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        readerListView.setAdapter(readerAdapter);
        readerListView.setLayoutManager(new LinearLayoutManager(getContext()));

        return view;
    }

    private class DeviceConnectTask extends AsyncTask<Void, String, Boolean> {
        private final ReaderDevice connectingDevice;
        private String prgressMsg;
        private OperationFailureException ex;
        private String password;
        private Fragment fragment;

        DeviceConnectTask(ReaderDevice connectingDevice, String prgressMsg, String Password, Fragment fragment) {
            this.connectingDevice = connectingDevice;
            this.prgressMsg = prgressMsg;
            password = Password;
            this.fragment = fragment;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new CustomProgressDialog(getActivity(), prgressMsg);
            progressDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... a) {
            try {
                if (password != null)
                    connectingDevice.getRFIDReader().setPassword(password);
                connectingDevice.getRFIDReader().connect();
                if (password != null) {
                    SharedPreferences.Editor editor = getActivity().getSharedPreferences("Security", 0).edit();
                    editor.putString(connectingDevice.getName(), password);
                    editor.commit();
                }
            } catch (InvalidUsageException e) {
                e.printStackTrace();
            } catch (OperationFailureException e) {
                e.printStackTrace();
                ex = e;
            }
            if (connectingDevice.getRFIDReader().isConnected()) {
                Global.connectedReader = connectingDevice.getRFIDReader();
                Global.connectedReader.Events.setTagReadEvent(true);
                /*try {
                    if (callerActivity.equals("EntryActivity"))
                        Global.connectedReader.Events.addEventsListener(Global.eventStockCheckHandler);
                    else if (callerActivity.equals("KillRFIDActivity"))
                        Global.connectedReader.Events.addEventsListener(Global.eventKillRFIDHandler);
                    Global.connectedReader.Events.setTagReadEvent(true);

                } catch (InvalidUsageException e) {
                    e.printStackTrace();
                } catch (OperationFailureException e) {
                    e.printStackTrace();
                }*/

                // if no exception in connect
                try {
                    String channels = "921250,921750,924250,923750,920750,922250,923250,922750";
                    RegulatoryConfig regulatoryConfig = new RegulatoryConfig();
                    regulatoryConfig.setRegion("SGP");
                    regulatoryConfig.setEnabledChannels(channels.split(","));
                    Global.connectedReader.Config.setRegulatoryConfig(regulatoryConfig);
                } catch (InvalidUsageException e) {
                    e.printStackTrace();
                    return false;
                } catch (OperationFailureException e) {
                    e.printStackTrace();
                    return false;
                }
                return true;
            } else {
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            progressDialog.cancel();
            if (ex != null) {
                if (ex.getResults() == RFIDResults.RFID_CONNECTION_PASSWORD_ERROR) {
                    Toast.makeText(getActivity(), "Password required/wrong", Toast.LENGTH_SHORT).show();
                } else if (ex.getResults() == RFIDResults.RFID_BATCHMODE_IN_PROGRESS) {
                    Toast.makeText(getActivity(), "Batch node in progress", Toast.LENGTH_SHORT).show();
                } else if (ex.getResults() == RFIDResults.RFID_READER_REGION_NOT_CONFIGURED) {
                    Toast.makeText(getActivity(), "RFID region not configured.", Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(getActivity(), "Failed to connect", Toast.LENGTH_SHORT).show();
            } else {
                if (result) {
                    getActivity().getFragmentManager().beginTransaction().remove(fragment).commit();
                    Toast.makeText(getActivity(), "Connected", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity(), "Failed to connect", Toast.LENGTH_SHORT).show();
                }
            }
            deviceConnectTask = null;
        }

        @Override
        protected void onCancelled() {
            deviceConnectTask = null;
            super.onCancelled();
        }

    }
}
