package com.easipos.rfid.rfidinventory;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class KillRFIDTagRecordsAdapter extends RecyclerView.Adapter<KillRFIDTagViewHolder> {

    List<KillTagData> list = Collections.emptyList();
    Context context;
    CustomRecyclerViewClickListener listener;

    public KillRFIDTagRecordsAdapter(List<KillTagData> list, CustomRecyclerViewClickListener listener) {
        this.list = list;
        this.listener = listener;
    }

    public KillRFIDTagViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.kill_rfid_tag_row_layout, parent, false);
        final KillRFIDTagViewHolder holder = new KillRFIDTagViewHolder(v);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, holder.getAdapterPosition());
            }
        });
        return holder;

    }

    @Override
    public void onBindViewHolder(KillRFIDTagViewHolder holder, final int position) {

        //Use the provided View Holder on the onCreateViewHolder method to populate the current row on the RecyclerView
        holder.checkBox.setOnCheckedChangeListener(null);
        holder.tid.setText(list.get(position).getID());
        holder.productCode.setText(list.get(position).getCode());
        holder.productName.setText(list.get(position).getName());
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                list.get(position).setChecked(isChecked);
            }
        });

        if (list.get(position).getChecked()) {
            holder.setCheckBox(true);
        }
        else {
            holder.setCheckBox(false);
        }

        //animate(holder);
    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return list.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    // Insert a new item to the RecyclerView on a predefined position
    public void insert(int position, KillTagData data) {
        list.add(position, data);
        //notifyItemInserted(position);
    }

    // Remove a RecyclerView item containing a specified Data object
    public void removeAll() {

        for(int i = list.size()-1 ; i>=0 ; i--) {
            list.remove(i);
        }
        notifyDataSetChanged();
    }

    public void toggleSelectAll()
    {
        boolean isAllChecked = true;
        for(int i = 0 ; i<list.size() && isAllChecked == true ; i++) {
            if(!list.get(i).getChecked())
                isAllChecked = false;
        }

        if(isAllChecked==true){
            for(int i = 0; i<list.size() ; i++)
                list.get(i).setChecked(false);
        }
        else
        {
            for(int i = 0; i<list.size() ; i++)
                list.get(i).setChecked(true);
        }
        notifyDataSetChanged();
    }

    public List<String> getCheckedTID()
    {
        List<String> TIDList = new ArrayList<>();
        for(int i = 0 ; i<getItemCount() ; i++)
            if(list.get(i).getChecked())
                TIDList.add(list.get(i).getID());

        return TIDList;
    }

    public boolean isTIDExistsInList(String code)
    {
        for(int i = 0 ; i<getItemCount() ; i++)
            if(list.get(i).getID().equals(code))
                return true;
        return false;
    }
}
