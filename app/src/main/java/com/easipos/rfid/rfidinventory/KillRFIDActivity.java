package com.easipos.rfid.rfidinventory;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.zebra.rfid.api3.ACCESS_OPERATION_CODE;
import com.zebra.rfid.api3.ACCESS_OPERATION_STATUS;
import com.zebra.rfid.api3.InvalidUsageException;
import com.zebra.rfid.api3.OperationFailureException;
import com.zebra.rfid.api3.ReaderDevice;
import com.zebra.rfid.api3.RfidEventsListener;
import com.zebra.rfid.api3.RfidReadEvents;
import com.zebra.rfid.api3.RfidStatusEvents;
import com.zebra.rfid.api3.TagData;

import java.util.ArrayList;
import java.util.List;

public class KillRFIDActivity extends AppCompatActivity implements ResponseHandlerInterfaces.ResponseKillTagHandler, StockCheckRecordsListFragment.StockCheckRecordSelectedListener {

    private Button startStopScanButton,killRFIDButton,clearListButton,selectAllButton;
    private RecyclerView killRFIDTagListView;
    private List<KillTagData> killRFIDTagList;
    private CustomProgressDialog progressDialog;
    private DBHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kill_rfid);

        db = new DBHelper(KillRFIDActivity.this);
        Global.eventKillRFIDHandler = new EventHandler();
        killRFIDTagList = new ArrayList<>();

        RetrieveRFIDKillMasterTask task = new RetrieveRFIDKillMasterTask();
        task.execute();

        killRFIDButton = findViewById(R.id.kill_RFID_button);
        startStopScanButton = findViewById(R.id.start_stop_read_kill_RFID_button);
        killRFIDTagListView = findViewById(R.id.kill_RFID_tag_list_view);
        clearListButton = findViewById(R.id.clear_list);
        selectAllButton = findViewById(R.id.select_all);

        clearListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Global.killRFIDTagListAdapter.removeAll();
            }
        });

        selectAllButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Global.killRFIDTagListAdapter.toggleSelectAll();
            }
        });

        startStopScanButton.setText("START SCAN");
        startStopScanButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (startStopScanButton.getText().equals("START SCAN")) {
                        if (BluetoothAdapter.getDefaultAdapter().isEnabled()) {
                            //open list of reader page if device is not connected
                            if (Global.connectedReader != null && Global.connectedReader.isConnected()) {
                                try {
                                    Global.connectedReader.Events.removeEventsListener(Global.eventStockCheckHandler);
                                } catch (InvalidUsageException e) {
                                    e.printStackTrace();
                                } catch (OperationFailureException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    Global.connectedReader.Events.addEventsListener(Global.eventKillRFIDHandler);
                                    Global.connectedReader.Actions.Inventory.perform();
                                    killRFIDButton.setEnabled(false) ;
                                    clearListButton.setEnabled(false);
                                    selectAllButton.setEnabled(false);
                                    startStopScanButton.setText("STOP SCAN");
                                } catch (InvalidUsageException e) {
                                    e.printStackTrace();
                                } catch (final OperationFailureException e) {
                                    e.printStackTrace();
                                    Toast.makeText(getBaseContext(), "Status Notification: " + e.getVendorMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                // list out readers.
                                openReadersPage(startStopScanButton);
                            }
                        } else {
                            Toast.makeText(getBaseContext(), "Bluetooth is turned off. Please switch it on to use this feature.", Toast.LENGTH_SHORT).show();
                        }

                } else
                    try {
                        Global.connectedReader.Actions.Inventory.stop();
                        Global.connectedReader.Events.removeEventsListener(Global.eventKillRFIDHandler);
                        killRFIDButton.setEnabled(true) ;
                        clearListButton.setEnabled(true);
                        selectAllButton.setEnabled(true);
                        startStopScanButton.setText("START SCAN");
                    } catch (InvalidUsageException e) {
                        e.printStackTrace();
                    } catch (OperationFailureException e) {
                        e.printStackTrace();
                    }
            }
        });

        killRFIDButton.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                  final List<String> returnedTIDList = Global.killRFIDTagListAdapter.getCheckedTID();

                  if(returnedTIDList.size()==0)
                      Toast.makeText(KillRFIDActivity.this, "No Item Checked", Toast.LENGTH_SHORT).show();
                  else
                  {
                      String message = new String();
                      for(int i = 1; i<=returnedTIDList.size() ; i++) {
                          message = message + returnedTIDList.get(i-1);
                          if(i != returnedTIDList.size())
                              message = message + ",";
                          if(i%5==0)
                          {
                              message = message + "\n";
                          }
                      }

                      AlertDialog.Builder builder = new AlertDialog.Builder(KillRFIDActivity.this);

                      builder.setTitle("De-activate RFID Tags");
                      builder.setMessage("Action cannot be undo!!\nYES to proceed, NO to cancel.\n\nTag(s) to be removed:\n"+message);

                      builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                          public void onClick(DialogInterface dialog, int which) {
                              dialog.dismiss();
                              KillRFIDTask task = new KillRFIDTask(returnedTIDList);
                              task.execute();
                          }
                      });

                      builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                          @Override
                          public void onClick(DialogInterface dialog, int which) {
                              dialog.dismiss();
                          }
                      });

                      builder.setCancelable(false);

                      AlertDialog alert = builder.create();
                      alert.setCanceledOnTouchOutside(false);
                      alert.show();
                  }
              }
          }
        );

        if(Global.killRFIDTagListAdapter == null)
            Global.killRFIDTagListAdapter = new KillRFIDTagRecordsAdapter(killRFIDTagList,new CustomRecyclerViewClickListener() {

                @Override
                public void onItemClick(View v, int position) {
                    // do what ever you want to do with it
                    if (killRFIDTagList.get(position).getChecked()) {
                        killRFIDTagList.get(position).setChecked(false);
                    }
                    else {
                        killRFIDTagList.get(position).setChecked(true);
                    }
                    Global.killRFIDTagListAdapter.notifyDataSetChanged();
                }
            });

        killRFIDTagListView.setAdapter(Global.killRFIDTagListAdapter);
        killRFIDTagListView.setLayoutManager(new LinearLayoutManager(this));

    }

    @Override
    public void responseKillTagHandler(KillTagData tagDataToBeEnter) {
        if(!Global.killRFIDTagListAdapter.isTIDExistsInList(tagDataToBeEnter.getID())) {
            Global.killRFIDTagListAdapter.insert(Global.killRFIDTagListAdapter.getItemCount(), tagDataToBeEnter);
            Global.killRFIDTagListAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onBackPressed()
    {
        if(startStopScanButton.getText().equals("STOP SCAN"))
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(KillRFIDActivity.this);

            builder.setTitle("Information");
            builder.setMessage("Reader is scanning, please stop the scan before exit.");

            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            builder.setCancelable(false);

            AlertDialog alert = builder.create();
            alert.setCanceledOnTouchOutside(false);
            alert.show();
        }
        else {
            db.deleteAllData("RFID_Kill_Master");
            Global.killRFIDTagListAdapter.removeAll();
            super.onBackPressed();
        }
    }

    @Override
    public void onRecordSelected(String refNo) {

    }

    public class EventHandler implements RfidEventsListener {

        @Override
        public void eventReadNotify(RfidReadEvents e) {

            final TagData[] myTags = Global.connectedReader.Actions.getReadTags(100);
            if (myTags != null) {
                for (int index = 0; index < myTags.length; index++) {
                    if (myTags[index].getOpCode() == ACCESS_OPERATION_CODE.ACCESS_OPERATION_READ &&
                            myTags[index].getOpStatus() == ACCESS_OPERATION_STATUS.ACCESS_SUCCESS) {
//                        if (myTags[index].getMemoryBankData().length() > 0) {
//                            System.out.println(" Mem Bank Data " + myTags[index].getMemoryBankData());
//                        }
                    }

                    if (myTags[index] != null && (myTags[index].getOpStatus() == null || myTags[index].getOpStatus() == ACCESS_OPERATION_STATUS.ACCESS_SUCCESS)) {
                        final int tag = index;
                        new ResponseHandlerTask(myTags[tag]).execute();
                    }

                }
            }
        }

        @Override
        public void eventStatusNotify(RfidStatusEvents rfidStatusEvents) {
            Toast.makeText(getBaseContext(), "Status Notification: " + rfidStatusEvents.StatusEventData.getStatusEventType(), Toast.LENGTH_SHORT).show();
        }
    }

    public void openReadersPage(View v) {
        // TableLayout settingsTable = (TableLayout) findViewById(R.id.settings_Table);

        Fragment myfragment;
        myfragment = new ReaderListFragment();
        ((ReaderListFragment) myfragment).callerActivity = "KillRFIDActivity";
        Global.readerListFragment = myfragment;
        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.killRFIDForm, myfragment);
        fragmentTransaction.commit();
    }

    public class KillRFIDTask extends AsyncTask<Void, Void, Boolean> {
        private String message;
        private List<String> tags;
        SOAPWeb.SOAPReturn killRFIDReturnedResult;

        public KillRFIDTask(List<String> tags)
        {
            this.tags = tags;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new CustomProgressDialog(KillRFIDActivity.this, "De-activating tags..Please wait..");
            progressDialog.show();
        }
        @Override
        protected Boolean doInBackground(Void... voids) {

            SOAPWeb soapWeb = new SOAPWeb(KillRFIDActivity.this);
            killRFIDReturnedResult = soapWeb.killRFID(tags);
            if (killRFIDReturnedResult.getResult()==false) {
                message = killRFIDReturnedResult.getMessage();
                return false;
            }
            else
            {
                return true;
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
            cancel(true);
            progressDialog.cancel();
            if(!result) {
                Toast.makeText(KillRFIDActivity.this, message, Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(KillRFIDActivity.this, "Tag(s) successfully de-activated!", Toast.LENGTH_SHORT).show();
            }
            db.deleteAllData("RFID_Kill_Master");
            Global.killRFIDTagListAdapter.removeAll();
            finish();
        }
    }

    public class RetrieveRFIDKillMasterTask extends AsyncTask<Void, Void, Boolean> {
        private String message;
        private SOAPWeb.SOAPReturnRFIDDataList returnedRFIData;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new CustomProgressDialog(KillRFIDActivity.this, "Establishing connection to server..");
            progressDialog.show();
        }
        @Override
        protected Boolean doInBackground(Void... voids) {

            SOAPWeb soapWeb = new SOAPWeb(KillRFIDActivity.this);
            returnedRFIData = soapWeb.DownloadRFIDList("","");
            if (returnedRFIData.getResult()==false) {
                message = returnedRFIData.getMessage();
                return false;
            }
            else
            {
                List<RFIDEntryData> data = returnedRFIData.getDataList();
                db.deleteAllData("RFID_Kill_Master");
                db.insertDataIntoRFIDKillMaster(data);
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            cancel(true);
            progressDialog.cancel();
            if(!result) {
                Toast.makeText(KillRFIDActivity.this, message, Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    public class ResponseHandlerTask extends AsyncTask<Void, Void, Boolean> {
        private TagData tagData;
        private String epcInAscii;
        private KillTagData tagDataToBeEnter;
        SOAPWeb.SOAPReturnItemValidity returnedResult;

        ResponseHandlerTask(TagData tagData) {
            this.tagData = tagData;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            boolean result;
            List<String> characters;
            epcInAscii = new String();
            RFIDEntryData data;

            try {
                characters = Global.splitByLength(tagData.getTagID(), 2);
                epcInAscii = Global.convertHexToString(characters);
                data = db.getTagInfoFromRFIDKillMaster(epcInAscii);

                if(data==null)
                    return false;

                tagDataToBeEnter = new KillTagData(data.getID(),data.getCode(),data.getName());
                result = true;
            } catch (IndexOutOfBoundsException e) {
                //logAsMessage(TYPE_ERROR, TAG, e.getMessage());
                result = false;
            } catch (Exception e) {
                // logAsMessage(TYPE_ERROR, TAG, e.getMessage());
                result = false;
            }
            return result;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            cancel(true);
            if(result)
                responseKillTagHandler(tagDataToBeEnter);
            //if (oldObject != null && fragment instanceof ResponseHandlerInterfaces.ResponseTagHandler)
            //    ((ResponseHandlerInterfaces.ResponseTagHandler) fragment).handleTagResponse(epcInAscii);
            //oldObject = null;
        }
    }
}
