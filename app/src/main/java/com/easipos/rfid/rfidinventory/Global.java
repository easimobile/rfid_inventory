package com.easipos.rfid.rfidinventory;



import android.app.Fragment;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;

import com.zebra.rfid.api3.RFIDReader;
import com.zebra.rfid.api3.ReaderDevice;
import com.zebra.rfid.api3.RegionInfo;
import com.zebra.rfid.api3.RegulatoryConfig;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class Global {
    public static RFIDReader connectedReader=null;
    public static Fragment readerListFragment;
    public static Fragment recordListFragment;
    public static RFIDAdapter entryAdapter;
    public static KillRFIDTagRecordsAdapter killRFIDTagListAdapter;
    public static DBHelper RFIDTableHelper;
    public static EntryActivity.EventHandler eventStockCheckHandler;
    public static KillRFIDActivity.EventHandler eventKillRFIDHandler;
    public static RecyclerView entryList;


    public static List<String> splitByLength(String s, int length)
    {
        List<String> splitString = new ArrayList<>();
        int len = s.length();

        for(int x = 0, y = length ; x < len ; x+=length , y+=length)
        {
            String hex = s.substring(x,y);
            if (hex.equals("00")==false)
            {
                splitString.add(hex);
            }
        }
        return splitString;
    }

    public static String convertHexToString(List<String> hex)
    {
        String ascii = new String();
        StringBuilder sb = new StringBuilder();
        int ival=0;

        for(int i = 0 ; i < hex.size() ; i++)
        {
            ival = Integer.parseInt(hex.get(i),16);
            sb.append((char)ival);
        }

        ascii = sb.toString();

        return ascii;
    }

}




