package com.easipos.rfid.rfidinventory;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.zebra.rfid.api3.InvalidUsageException;
import com.zebra.rfid.api3.OperationFailureException;
import com.zebra.rfid.api3.RFIDResults;
import com.zebra.rfid.api3.ReaderDevice;
import com.zebra.rfid.api3.Readers;

import java.util.ArrayList;
import java.util.List;

public class StockCheckRecordsListFragment extends Fragment {

    private List<String> recordList = new ArrayList<>();
    private RecyclerView recordListView;
    private StockCheckRecordsAdapter recordAdapter;
    private CustomProgressDialog progressDialog;
    private String location;
    private Context baseContext;
    private RetrieveStockCheckRecords retrieveStockCheckRecords;
    private SOAPWeb.SOAPReturnStringList returnedData;
    StockCheckRecordSelectedListener mCallback;

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_stockcheck_record_list, container, false);

        recordListView = view.findViewById(R.id.stock_check_record_list_view);
        recordList.clear();

        recordAdapter = new StockCheckRecordsAdapter(recordList,new CustomRecyclerViewClickListener() {

            @Override
            public void onItemClick(View v, int position) {
                mCallback.onRecordSelected(recordList.get(position));
                getActivity().getFragmentManager().beginTransaction().remove(Global.recordListFragment).commit();
                }
        });

        recordListView.setAdapter(recordAdapter);
        recordListView.setLayoutManager(new LinearLayoutManager(getContext()));

        retrieveStockCheckRecords = new RetrieveStockCheckRecords("Loading..");
        retrieveStockCheckRecords.execute((Void)null);

        return view;
    }

    public void setEnvironmentalVariables(String location,Context context, StockCheckRecordSelectedListener mCallback)
    {
        this.location = location.substring(0, location.indexOf("-"));
        this.baseContext = context;
        this.mCallback = mCallback;
    }

    private class RetrieveStockCheckRecords extends AsyncTask<Void, String, Boolean> {
        private String Msg;

        RetrieveStockCheckRecords(String progressMsg) {
            this.Msg = progressMsg;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new CustomProgressDialog(getActivity(), Msg);
            progressDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... a) {

            SOAPWeb soapweb = new SOAPWeb(baseContext);
            returnedData = soapweb.retrieveRecordListByLoc(location);
            if(returnedData.getResult()) {
                recordList = returnedData.getDataList();
                return true;
            }
            else
                return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            if(result) {

                for (int i = 0; i < recordList.size(); i++)
                    recordAdapter.insert(recordAdapter.getItemCount(), recordList.get(i));

                recordListView.setAdapter(recordAdapter);
                recordAdapter.notifyDataSetChanged();
                progressDialog.cancel();
            }
            else {
                progressDialog.cancel();
                getActivity().getFragmentManager().beginTransaction().remove(Global.recordListFragment).commit();
                Toast.makeText(baseContext, returnedData.getMessage(), Toast.LENGTH_SHORT).show();
            }

            retrieveStockCheckRecords = null;
        }

        @Override
        protected void onCancelled() {
            retrieveStockCheckRecords = null;
            super.onCancelled();
        }
    }

    public interface StockCheckRecordSelectedListener{
        void onRecordSelected(String refNo);
    }
}
