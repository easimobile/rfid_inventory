package com.easipos.rfid.rfidinventory;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpResponseException;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class SOAPWeb {
    private String SOAP_ACTION;
    private String METHOD_NAME;
    private static String NAMESPACE = "http://tempuri.org/";
    private String webServiceURL;
    private Context context;

    public SOAPWeb(Context context) {
        this.context = context;
        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(context);
        webServiceURL = SP.getString("webServiceURL", "NA");
        SOAP_ACTION = NAMESPACE + METHOD_NAME;
    }

    /**
     * Validate user input ID and password against web server -> DB
     *
     * @param id       = plain text ID
     * @param password = plain text PASSWORD
     * @return "Y" = Success else Fail
     */

    public SOAPReturn ValidateEmployeeCredentials(String id, String password) {
        METHOD_NAME = "SelectEmployee";
        SOAP_ACTION = NAMESPACE + METHOD_NAME;

        SOAPReturn soapReturn;

        EmployeeRequest er = new EmployeeRequest(id, false, password, "");
        PropertyInfo pi = new PropertyInfo();
        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

        pi.setName("employee");
        pi.setValue(er);
        pi.setType(EmployeeRequest.class);

        request.addProperty(pi);

        envelope.implicitTypes = true;
        envelope.setOutputSoapObject(request);
        envelope.dotNet = true;

        HttpTransportSE httpTransport = new HttpTransportSE(webServiceURL);
        httpTransport.debug = true;
        try {
            httpTransport.call(SOAP_ACTION, envelope);
            Log.d("Dump",httpTransport.requestDump);
            Log.d("Dump",httpTransport.responseDump);
        } catch (HttpResponseException e) {
            // TODO Auto-generated catch block
            Log.e("HttpResponseException", e.getMessage());
            e.printStackTrace();
            soapReturn = new SOAPReturn(false,e.getMessage());
            return soapReturn;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            Log.e("IOException", e.getMessage());
            e.printStackTrace();
            soapReturn = new SOAPReturn(false,e.getMessage());
            return soapReturn;
        } catch (XmlPullParserException e) {
            // TODO Auto-generated catch block
            Log.e("XmlPullParserException", e.getMessage());
            e.printStackTrace();
            soapReturn = new SOAPReturn(false,e.getMessage());
            return soapReturn;
        } //send request

        SoapObject result = null;
        try {
            result = (SoapObject) envelope.bodyIn;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e("SOAP_SoapFault", e.getMessage());
            e.printStackTrace();
            soapReturn = new SOAPReturn(false,e.getMessage());
            return soapReturn;
        }

        String test = String.valueOf(result.getProperty("SelectEmployeeResult")).trim();
        boolean test2 = test.equals("");

        if (String.valueOf(result.getProperty("SelectEmployeeResult")).trim().equals("Y"))
            soapReturn = new SOAPReturn(true,"");
        else if (String.valueOf(result.getProperty("SelectEmployeeResult")).trim().equals("anyType{}"))
            soapReturn = new SOAPReturn(false,"ID/Password incorrect.");
        else
            soapReturn = new SOAPReturn(false,String.valueOf(result.getProperty("errorMessage")).trim());

        return soapReturn;
    }

    /**
     * Get locations from web service.
     * @return List of locations.
     */
    public ArrayList<String> DownloadStockLocations() {

        ArrayList<String> locationList = new ArrayList<>();

        METHOD_NAME = "GetLocation";
        SOAP_ACTION = NAMESPACE + METHOD_NAME;
        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

        envelope.implicitTypes = true;
        envelope.setOutputSoapObject(request);
        envelope.dotNet = true;

        HttpTransportSE httpTransport = new HttpTransportSE(webServiceURL);
        httpTransport.debug = true;
        try {
            httpTransport.call(SOAP_ACTION, envelope);
        } catch (HttpResponseException e) {
            // TODO Auto-generated catch block
            Log.e("HttpResponseException", e.getMessage());
            e.printStackTrace();
            locationList.add(e.getMessage());
            return locationList;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            Log.e("IOException", e.getMessage());
            e.printStackTrace();
            locationList.add(e.getMessage());
            return locationList;
        } catch (XmlPullParserException e) {
            // TODO Auto-generated catch block
            Log.e("XmlPullParserException", e.getMessage());
            e.printStackTrace();
            locationList.add(e.getMessage());
            return locationList;
        } //send request

        SoapObject responseBody = null;
        try {
            responseBody = (SoapObject) envelope.bodyIn;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e("SOAP_SoapFault", e.getMessage());
            e.printStackTrace();
            locationList.add(e.getMessage());
            return locationList;
        }
        SoapObject soap1 = (SoapObject) responseBody.getProperty(0);
        SoapObject locations = (SoapObject) soap1.getProperty(1);
        SoapObject looplocations;

        locationList.add("<Location>");

        for(int i = 0 ; i < locations.getPropertyCount() ; i++)
        {
            looplocations = (SoapObject) locations.getProperty(i);
            locationList.add(String.valueOf(looplocations.getProperty("LocCode")).trim()+"-"+String.valueOf(looplocations.getProperty("LocName")).trim());
        }

        return locationList;
    }

    public SOAPReturnRFIDDataList DownloadRFIDList(String location,String referenceNumber)
    {
        SOAPReturnRFIDDataList soapReturn;
        ArrayList<RFIDEntryData> dataList = new ArrayList<>();

        METHOD_NAME = "GetStockBalance";
        SOAP_ACTION = NAMESPACE + METHOD_NAME;

        PropertyInfo pi = new PropertyInfo();
        pi.setName("stk_loc");
        pi.setValue(location);
        pi.setType(String.class);

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

        request.addProperty(pi);

        pi = new PropertyInfo();
        pi.setName("referenceNo");
        pi.setValue(referenceNumber);
        pi.setType(String.class);

        request.addProperty(pi);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

        envelope.implicitTypes = true;
        envelope.setOutputSoapObject(request);
        envelope.dotNet = true;

        HttpTransportSE httpTransport = new HttpTransportSE(webServiceURL);
        httpTransport.debug = true;
        try {
            httpTransport.call(SOAP_ACTION, envelope);

            Log.d("Dump",httpTransport.requestDump);
            Log.d("Dump",httpTransport.responseDump);
        } catch (HttpResponseException e) {
            // TODO Auto-generated catch block
            Log.e("HttpResponseException", e.getMessage());
            e.printStackTrace();
            soapReturn = new SOAPReturnRFIDDataList(false, e.getMessage(),null);
            return soapReturn;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            Log.e("IOException", e.getMessage());
            e.printStackTrace();
            soapReturn = new SOAPReturnRFIDDataList(false, e.getMessage(),null);
            return soapReturn;
        } catch (XmlPullParserException e) {
            // TODO Auto-generated catch block
            Log.e("XmlPullParserException", e.getMessage());
            e.printStackTrace();
            soapReturn = new SOAPReturnRFIDDataList(false, e.getMessage(),null);
            return soapReturn;
        } //send request

        SoapObject responseBody = null;
        try {
            responseBody = (SoapObject) envelope.bodyIn;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e("SOAP_SoapFault", e.getMessage());
            e.printStackTrace();
            soapReturn = new SOAPReturnRFIDDataList(false, e.getMessage(),null);
            return soapReturn;
        }
        SoapObject soap1 = (SoapObject) responseBody.getProperty(0);

        if (String.valueOf(soap1.getProperty(0)).equals("False"))
            return new SOAPReturnRFIDDataList(false,"Problem occurred, please try again.",null);

        SoapObject RFIDItems = (SoapObject) soap1.getProperty(1);
        SoapObject loopRFIDItem;

        for(int i = 0 ; i < RFIDItems.getPropertyCount() ; i++)
        {
            loopRFIDItem = (SoapObject) RFIDItems.getProperty(i);

            dataList.add(new RFIDEntryData(String.valueOf(loopRFIDItem.getProperty("TagId")).trim(),String.valueOf(loopRFIDItem.getProperty("Code")).trim(),String.valueOf(loopRFIDItem.getProperty("Name")).trim()));
        }
        soapReturn = new SOAPReturnRFIDDataList(true, null,dataList);
        return soapReturn;
    }

    public String createStockCheckRecord(String location)
    {
        String refNumber;

        METHOD_NAME = "CreateMasStkCheck";
        SOAP_ACTION = NAMESPACE + METHOD_NAME;

        PropertyInfo pi = new PropertyInfo();
        pi.setName("stk_loc");
        pi.setValue(location);
        pi.setType(String.class);

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

        request.addProperty(pi);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

        envelope.implicitTypes = true;
        envelope.setOutputSoapObject(request);
        envelope.dotNet = true;

        HttpTransportSE httpTransport = new HttpTransportSE(webServiceURL);
        httpTransport.debug = true;
        try {
            httpTransport.call(SOAP_ACTION, envelope);
        } catch (HttpResponseException e) {
            // TODO Auto-generated catch block
            Log.e("HttpResponseException", e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            Log.e("IOException", e.getMessage());
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            // TODO Auto-generated catch block
            Log.e("XmlPullParserException", e.getMessage());
            e.printStackTrace();
        } //send request

        SoapObject responseBody = null;
        try {
            responseBody = (SoapObject) envelope.bodyIn;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e("SOAP_SoapFault", e.getMessage());
            e.printStackTrace();
        }
        SoapObject soap1 = (SoapObject) responseBody.getProperty(0);
        refNumber = String.valueOf(soap1.getProperty("Tablename")).trim();

        if(String.valueOf(soap1.getProperty("status")).trim().equals("True"))
            return refNumber;
        return null;
    }

    public SOAPReturn updateStockCheckResultToWeb(List<StockCountResultItem> countList,List<MissingRFIDItem> missingList )
    {
        SOAPReturn soapReturn;
        METHOD_NAME = "UpdateStock";
        SOAP_ACTION = NAMESPACE + METHOD_NAME;

        SharedPreferences preferences = context.getSharedPreferences("StockCheck", MODE_PRIVATE);
        String refNo = preferences.getString("RefNo", "NONE");
        String stkLocation = preferences.getString("StkLoc", "NONE");

        List<Stock> stock = new ArrayList<>();
        List<RfidLabel> rfidLabel = new ArrayList<>();


        for(int i = 0 ; i < countList.size() ; i++)
            stock.add(new Stock(countList.get(i).getProductCode(),countList.get(i).getQuantity()));

        for(int i = 0 ; i < missingList.size() ; i++)
            rfidLabel.add(new RfidLabel(missingList.get(i).getTid(),missingList.get(i).getUserMemoryBank()));

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

        PropertyInfo pi = new PropertyInfo();
        pi.setName("referenceNo");
        pi.setValue(refNo);
        pi.setType(String.class);
        request.addProperty(pi);

        pi = new PropertyInfo();
        pi.setName("stk_loc");
        pi.setValue(stkLocation);
        pi.setType(String.class);
        request.addProperty(pi);

        SoapObject found = new SoapObject(null,"Found");
        for(int i = 0 ; i < stock.size() ; i ++)
        {
            found.addProperty("Stock",stock.get(i));
        }

        request.addSoapObject(found);

        SoapObject notFound = new SoapObject(null,"NotFound");

        for(int i = 0 ; i < rfidLabel.size() ; i ++)
        {
            notFound.addProperty("RfidLabel",rfidLabel.get(i));
        }

        request.addSoapObject(notFound);

        SoapObject extra = new SoapObject(null,"Extra");
        extra.addProperty("RfidLabel",new RfidLabel("?","?"));
        request.addSoapObject(extra);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

        envelope.implicitTypes = true;
        envelope.setOutputSoapObject(request);
        envelope.dotNet = true;

        HttpTransportSE httpTransport = new HttpTransportSE(webServiceURL);
        httpTransport.debug = true;
        try {
            httpTransport.call(SOAP_ACTION, envelope);
            Log.d("Dump",httpTransport.requestDump);
            Log.d("Dump",httpTransport.responseDump);

        } catch (HttpResponseException e) {
            // TODO Auto-generated catch block
            Log.e("HttpResponseException", e.getMessage());
            e.printStackTrace();
            soapReturn = new SOAPReturn(false,e.getMessage());
            return soapReturn;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            Log.e("IOException", e.getMessage());
            e.printStackTrace();
            soapReturn = new SOAPReturn(false,e.getMessage());
            return soapReturn;
        } catch (XmlPullParserException e) {
            // TODO Auto-generated catch block
            Log.e("XmlPullParserException", e.getMessage());
            e.printStackTrace();
            soapReturn = new SOAPReturn(false,e.getMessage());
            return soapReturn;
        } //send request

        SoapObject responseBody = null;
        try {
            responseBody = (SoapObject) envelope.bodyIn;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e("SOAP_SoapFault", e.getMessage());
            e.printStackTrace();
            soapReturn = new SOAPReturn(false,e.getMessage());
            return soapReturn;
        }

        SoapObject soap1 = (SoapObject) responseBody.getProperty(0);
        String result = String.valueOf(soap1.getProperty("status"));

        if (result.trim().equals("True"))
            soapReturn = new SOAPReturn(true, null);
        else
            soapReturn = new SOAPReturn(false, "Internal server error. Kindly contact support.");

        return soapReturn;
    }

    public SOAPReturn killRFID(List<String> tags)
    {
        SOAPReturn soapReturn;
        METHOD_NAME = "DeleteRFID";
        SOAP_ACTION = NAMESPACE + METHOD_NAME;

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

        SoapObject TagId = new SoapObject(null,"TagId");
        for(int i = 0 ; i < tags.size() ; i ++)
        {
            TagId.addProperty("string",tags.get(i));
        }

        SoapObject req = new SoapObject(null,"req");
        req.addSoapObject(TagId);

        PropertyInfo pi = new PropertyInfo();
        pi.setName("stkloc");
        pi.setValue("");
        pi.setType(String.class);
        req.addProperty(pi);

        request.addSoapObject(req);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

        envelope.implicitTypes = true;
        envelope.setOutputSoapObject(request);
        envelope.dotNet = true;

        HttpTransportSE httpTransport = new HttpTransportSE(webServiceURL);
        httpTransport.debug = true;
        try {
            httpTransport.call(SOAP_ACTION, envelope);
            Log.d("Dump",httpTransport.requestDump);
            Log.d("Dump",httpTransport.responseDump);

        } catch (HttpResponseException e) {
            // TODO Auto-generated catch block
            Log.e("HttpResponseException", e.getMessage());
            e.printStackTrace();
            soapReturn = new SOAPReturn(false,e.getMessage());
            return soapReturn;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            Log.e("IOException", e.getMessage());
            e.printStackTrace();
            soapReturn = new SOAPReturn(false,e.getMessage());
            return soapReturn;
        } catch (XmlPullParserException e) {
            // TODO Auto-generated catch block
            Log.e("XmlPullParserException", e.getMessage());
            e.printStackTrace();
            soapReturn = new SOAPReturn(false,e.getMessage());
            return soapReturn;
        } //send request

        SoapObject responseBody = null;
        try {
            responseBody = (SoapObject) envelope.bodyIn;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e("SOAP_SoapFault", e.getMessage());
            e.printStackTrace();
            soapReturn = new SOAPReturn(false,e.getMessage());
            return soapReturn;
        }

        SoapObject soap1 = (SoapObject) responseBody.getProperty(0);
        String result = String.valueOf(soap1.getProperty("status"));

        if (result.trim().equals("True"))
            soapReturn = new SOAPReturn(true, null);
        else
            soapReturn = new SOAPReturn(false, "Internal server error. Please try again.");

        return soapReturn;
    }

    public SOAPReturnStringList retrieveRecordListByLoc(String location)
    {
        SOAPReturnStringList soapReturn;
        ArrayList<String> dataList = new ArrayList<>();

        METHOD_NAME = "GetTablename";
        SOAP_ACTION = NAMESPACE + METHOD_NAME;

        PropertyInfo pi = new PropertyInfo();
        pi.setName("stk_loc");
        pi.setValue(location);
        pi.setType(String.class);

        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

        request.addProperty(pi);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

        envelope.implicitTypes = true;
        envelope.setOutputSoapObject(request);
        envelope.dotNet = true;

        HttpTransportSE httpTransport = new HttpTransportSE(webServiceURL);
        httpTransport.debug = true;
        try {
            httpTransport.call(SOAP_ACTION, envelope);
        } catch (HttpResponseException e) {
            // TODO Auto-generated catch block
            Log.e("HttpResponseException", e.getMessage());
            e.printStackTrace();
            soapReturn = new SOAPReturnStringList(false,e.getMessage(),null);
            return soapReturn;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            Log.e("IOException", e.getMessage());
            e.printStackTrace();
            soapReturn = new SOAPReturnStringList(false,e.getMessage(),null);
            return soapReturn;
        } catch (XmlPullParserException e) {
            // TODO Auto-generated catch block
            Log.e("XmlPullParserException", e.getMessage());
            e.printStackTrace();
            soapReturn = new SOAPReturnStringList(false,e.getMessage(),null);
            return soapReturn;
        } //send request

        SoapObject responseBody = null;
        try {
            responseBody = (SoapObject) envelope.bodyIn;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e("SOAP_SoapFault", e.getMessage());
            e.printStackTrace();
            soapReturn = new SOAPReturnStringList(false,e.getMessage(),null);
            return soapReturn;
        }
        SoapObject result = (SoapObject) responseBody.getProperty(0);

        if (String.valueOf(result.getProperty(0)).trim().equals("False")) {
            soapReturn = new SOAPReturnStringList(false,"Failed to load stock check records.",null);
            return soapReturn;
        }

        SoapObject tableNameList = (SoapObject) result.getProperty(1);
        SoapObject tableName;

        for(int i = 0 ; i < tableNameList.getPropertyCount() ; i++)
        {
            tableName = (SoapObject) tableNameList.getProperty(i);

            dataList.add(String.valueOf(tableName.getProperty(0)).trim());
        }

        soapReturn = new SOAPReturnStringList(true,null,dataList);
        return soapReturn;
    }

    /**
     *  Class for SOAP request/response
     */

    private class EmployeeRequest implements KvmSerializable {
        private String userID;
        private boolean posAllow;
        private String userPassword;
        private String salesOutletCode;

        EmployeeRequest(String id, boolean allowpos, String password, String outletcode){
            this.userID = id;
            this.posAllow = allowpos;
            this.userPassword = password;
            this.salesOutletCode = outletcode;
        }

        @Override
        public Object getProperty(int index) {
            switch(index)
            {
                case 0:
                    return userID;
                case 1:
                    return posAllow;
                case 2:
                    return userPassword;
                case 3:
                    return salesOutletCode;
            }

            return null;
        }

        @Override
        public int getPropertyCount() {
            return 4;
        }

        @Override
        public void setProperty(int index, Object value) {
            switch(index)
            {
                case 0:
                    userID = value.toString();
                    break;
                case 1:
                    posAllow = Boolean.parseBoolean(value.toString());
                    break;
                case 2:
                    userPassword = value.toString();
                    break;
                case 3:
                    salesOutletCode = value.toString();
                default:
                    break;
            }
        }

        @Override
        public void getPropertyInfo(int index, Hashtable properties, PropertyInfo info) {
            switch(index)
            {
                case 0:
                    info.type = PropertyInfo.STRING_CLASS;
                    info.name = "userID";
                    break;
                case 1:
                    info.type = PropertyInfo.BOOLEAN_CLASS;
                    info.name = "posAllow";
                    break;
                case 2:
                    info.type = PropertyInfo.STRING_CLASS;
                    info.name = "userPassword";
                    break;
                case 3:
                    info.type = PropertyInfo.STRING_CLASS;
                    info.name = "salesOutletCode";

                default:break;
            }
        }
    }

    private class Stock implements KvmSerializable {
        private String Prod_Cd;
        private int Quantity;

        Stock(String productCode, int quantity){
            this.Prod_Cd = productCode;
            this.Quantity = quantity;
        }

        @Override
        public Object getProperty(int index) {
            switch(index)
            {
                case 0:
                    return Prod_Cd;
                case 1:
                    return Quantity;
            }
            return null;
        }

        @Override
        public int getPropertyCount() {
            return 2;
        }

        @Override
        public void setProperty(int index, Object value) {
            switch(index)
            {
                case 0:
                    Prod_Cd = value.toString();
                    break;
                case 1:
                    Quantity = Integer.parseInt(value.toString());
                    break;
                default:
                    break;
            }
        }

        @Override
        public void getPropertyInfo(int index, Hashtable properties, PropertyInfo info) {
            switch(index)
            {
                case 0:
                    info.type = PropertyInfo.STRING_CLASS;
                    info.name = "Prod_Cd";
                    break;
                case 1:
                    info.type = PropertyInfo.INTEGER_CLASS;
                    info.name = "Quantity";
                    break;
                default:break;
            }
        }
    }

    private class TagId implements KvmSerializable {
        private String string;

        TagId(String string){
            this.string = string;
        }

        @Override
        public Object getProperty(int index) {
            switch(index)
            {
                case 0:
                    return string;
            }
            return null;
        }

        @Override
        public int getPropertyCount() {
            return 1;
        }

        @Override
        public void setProperty(int index, Object value) {
            switch(index)
            {
                case 0:
                    string = value.toString();
                    break;
                default:
                    break;
            }
        }

        @Override
        public void getPropertyInfo(int index, Hashtable properties, PropertyInfo info) {
            switch(index)
            {
                case 0:
                    info.type = PropertyInfo.STRING_CLASS;
                    info.name = "string";
                    break;
                default:break;
            }
        }
    }

    private class RfidLabel implements KvmSerializable {
        private String TagId;
        private String usermemorybank;

        RfidLabel(String TagId, String usermemorybank){
            this.TagId = TagId;
            this.usermemorybank = usermemorybank;
        }

        @Override
        public Object getProperty(int index) {
            switch(index)
            {
                case 0:
                    return TagId;
                case 1:
                    return usermemorybank;
            }
            return null;
        }

        @Override
        public int getPropertyCount() {
            return 2;
        }

        @Override
        public void setProperty(int index, Object value) {
            switch(index)
            {
                case 0:
                    TagId = value.toString();
                    break;
                case 1:
                    usermemorybank = value.toString();
                    break;
                default:
                    break;
            }
        }

        @Override
        public void getPropertyInfo(int index, Hashtable properties, PropertyInfo info) {
            switch(index)
            {
                case 0:
                    info.type = PropertyInfo.STRING_CLASS;
                    info.name = "TagId";
                    break;
                case 1:
                    info.type = PropertyInfo.STRING_CLASS;
                    info.name = "usermemorybank";
                    break;
                default:break;
            }
        }
    }

    public class SOAPReturn
    {
        private boolean result;
        private String message;

        SOAPReturn(boolean result,String message)
        {
            this.result=result;
            this.message=message;
        }

        public boolean getResult(){return result;}

        public String getMessage(){return message;}
    }

    public class SOAPReturnRFIDDataList
    {
        private boolean result;
        private String message;
        ArrayList<RFIDEntryData> dataList = new ArrayList<>();

        SOAPReturnRFIDDataList(boolean result, String message, ArrayList<RFIDEntryData> dataList)
        {
            this.result=result;
            this.message=message;
            this.dataList=dataList;
        }

        public boolean getResult(){return result;}

        public ArrayList<RFIDEntryData> getDataList(){return dataList;}

        public String getMessage(){return message;}
    }

    public class SOAPReturnStringList
    {
        private boolean result;
        private String message;
        ArrayList<String> dataList = new ArrayList<>();

        SOAPReturnStringList(boolean result, String message, ArrayList<String> dataList)
        {
            this.result=result;
            this.message=message;
            this.dataList=dataList;
        }

        public boolean getResult(){return result;}

        public ArrayList<String> getDataList(){return dataList;}

        public String getMessage(){return message;}
    }

    public class SOAPReturnItemValidity{
        private boolean result;
        private String message;
        private String epc;
        private String productCode;
        private String productName;

        SOAPReturnItemValidity(boolean result, String message, String epc, String productCode, String productName)
        {
            this.result = result;
            this.message = message;
            this.epc = epc;
            this.productCode = productCode;
            this.productName = productName;
        }

        public boolean getResult(){return this.result;}
        public String getEPC(){return this.epc;}
        public String getProductCode(){return this.productCode;}
        public String getProductName(){return this.productName;}
        public String getMessage(){return this.message;}
    }

}
