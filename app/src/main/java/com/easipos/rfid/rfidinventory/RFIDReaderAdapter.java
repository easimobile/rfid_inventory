package com.easipos.rfid.rfidinventory;

import android.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zebra.rfid.api3.ReaderDevice;

import java.util.Collections;
import java.util.List;

public class RFIDReaderAdapter extends RecyclerView.Adapter<Reader_List_Holder> {

    List<ReaderDevice> list = Collections.emptyList();
    CustomRecyclerViewClickListener listener;

    public RFIDReaderAdapter(List<ReaderDevice> list, CustomRecyclerViewClickListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @Override
    public Reader_List_Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.reader_row_layout, parent, false);
        final Reader_List_Holder holder = new Reader_List_Holder(v);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, holder.getAdapterPosition());
            }
        });
        return holder;

    }

    @Override
    public void onBindViewHolder(Reader_List_Holder holder, int position) {

        //Use the provided View Holder on the onCreateViewHolder method to populate the current row on the RecyclerView
        holder.id.setText(list.get(position).getName());
        holder.mac.setText(list.get(position).getAddress());

        //animate(holder);
    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return list.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    // Insert a new item to the RecyclerView on a predefined position
    public void insert(int position, ReaderDevice data) {
        list.add(position, data);
        //notifyItemInserted(position);
    }

    // Remove a RecyclerView item containing a specified Data object
    public void remove(ReaderDevice data) {

        int position = list.indexOf(data);
        list.remove(position);
        notifyItemRemoved(position);
    }
}

