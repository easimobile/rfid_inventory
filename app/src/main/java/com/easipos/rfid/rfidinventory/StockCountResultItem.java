package com.easipos.rfid.rfidinventory;

public class StockCountResultItem {
    private String productCode;
    private int Quantity;

    StockCountResultItem(String code, int quantity){
        this.productCode = code;
        this.Quantity = quantity;
    }

    public String getProductCode()
    {
        return this.productCode;
    }

    public int getQuantity()
    {
        return this.Quantity;
    }
}
