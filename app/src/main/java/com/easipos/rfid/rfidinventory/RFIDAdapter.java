package com.easipos.rfid.rfidinventory;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Collections;
import java.util.List;

public class RFIDAdapter extends RecyclerView.Adapter<View_Holder> {

    List<RFIDEntryData> list = Collections.emptyList();
    Context context;

    public RFIDAdapter(List<RFIDEntryData> list) {
        this.list = list;
    }

    @Override
    public View_Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.entry_row_layout, parent, false);
        View_Holder holder = new View_Holder(v);
        return holder;

    }

    @Override
    public void onBindViewHolder(View_Holder holder, int position) {

        //Use the provided View Holder on the onCreateViewHolder method to populate the current row on the RecyclerView
        holder.tid.setText(list.get(position).getID());
        holder.productCode.setText(list.get(position).getCode());
        holder.productName.setText(list.get(position).getName());

        //animate(holder);
    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return list.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    // Insert a new item to the RecyclerView on a predefined position
    public void insert(int position, RFIDEntryData data) {
        list.add(position, data);
        //notifyItemInserted(position);
    }

    // Remove a RecyclerView item containing a specified Data object
    public void remove(RFIDEntryData data) {

        int position = list.indexOf(data);
        list.remove(position);
        //notifyItemRemoved(position);
    }

    public RFIDEntryData searchByTID(String tid)
    {
        for(int i = 0 ; i<list.size() ; i++)
        {
            if(list.get(i).getID().equals(tid))
            {
                return list.get(i);
            }
        }
        return null;
    }

    public void clear()
    {
        list.clear();
    }
}
