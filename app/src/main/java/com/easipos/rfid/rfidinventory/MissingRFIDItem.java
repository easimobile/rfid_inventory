package com.easipos.rfid.rfidinventory;

public class MissingRFIDItem {
    private String tid;
    private String userMemoryBank;

    MissingRFIDItem(String tid ,String userMemoryBank){
        this.tid = tid;
        this.userMemoryBank = userMemoryBank;
    }

    public String getTid(){
        return this.tid;
    }

    public String getUserMemoryBank(){
        return this.userMemoryBank;
    }

}
