package com.easipos.rfid.rfidinventory;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
    }

    @Override
    public void onBackPressed(){
        AlertDialog.Builder builder = new AlertDialog.Builder(MenuActivity.this);

        builder.setTitle("Logout");
        builder.setMessage("Are you confirm to log out?");

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                MenuActivity.this.finish();

            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setCancelable(false);

        AlertDialog alert = builder.create();
        alert.setCanceledOnTouchOutside(false);
        alert.show();
    }

    public void openStockCheck(View v)
    {
        Intent intent = new Intent(MenuActivity.this, EntryActivity.class);
        startActivity(intent);
    }

    public void openKillRFID(View v)
    {
        Intent intent = new Intent(MenuActivity.this, KillRFIDActivity.class);
        startActivity(intent);
    }


}
