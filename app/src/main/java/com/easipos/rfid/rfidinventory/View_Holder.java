package com.easipos.rfid.rfidinventory;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class View_Holder extends RecyclerView.ViewHolder {

    TextView tid;
    TextView productCode;
    TextView productName;

    View_Holder(View itemView) {
        super(itemView);
        tid = (TextView) itemView.findViewById(R.id.tid);
        productCode = (TextView) itemView.findViewById(R.id.prodCode);
        productName = (TextView) itemView.findViewById(R.id.prodName);
    }
}
