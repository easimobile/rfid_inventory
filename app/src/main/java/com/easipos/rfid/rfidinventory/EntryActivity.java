package com.easipos.rfid.rfidinventory;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.zebra.rfid.api3.ACCESS_OPERATION_CODE;
import com.zebra.rfid.api3.ACCESS_OPERATION_STATUS;
import com.zebra.rfid.api3.InvalidUsageException;
import com.zebra.rfid.api3.OperationFailureException;
import com.zebra.rfid.api3.RfidEventsListener;
import com.zebra.rfid.api3.RfidReadEvents;
import com.zebra.rfid.api3.RfidStatusEvents;
import com.zebra.rfid.api3.TagData;

import java.util.ArrayList;
import java.util.List;

public class EntryActivity extends AppCompatActivity implements ResponseHandlerInterfaces.ResponseTagHandler, StockCheckRecordsListFragment.StockCheckRecordSelectedListener {

    private RecyclerView entryList;
    private View mProgressView;
    private View entryFormView;
    private Spinner LocationSpinner;
    private Button refreshButton, newButton, loadButton, startStopRFID, updateStockCount;
    private TextView refNumberText,foundText,notFoundText;

    private RetrieveStockLocationsTask rStockLocTask = null;
    private RetrieveRFIDListTask rRFIDListTask = null;
    private LoadRFIDListTask lRFIDListTask = null;
    private UpdateStockCountTask updateStockCountTask = null;

    private ArrayList<String> LocationLists;
    private ArrayAdapter<String> adapter;

    private List<RFIDEntryData> rfidListData = new ArrayList<>();
    private Context baseContext;
    private DBHelper RFIDTableHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entry);

        /**
         * Initializing and get components' references.
         */
        RFIDTableHelper = new DBHelper(this);
        baseContext = getBaseContext();
        refNumberText = findViewById(R.id.stock_check_ref);
        mProgressView = findViewById(R.id.data_progress);
        startStopRFID = findViewById(R.id.start_stop_read_RFID_button);
        updateStockCount = findViewById(R.id.update_stock_count_button);
        entryFormView = findViewById(R.id.entryForm);
        LocationSpinner = findViewById(R.id.stock_location_choice);
        refreshButton = findViewById(R.id.location_refresh);
        entryList = findViewById(R.id.item_list_view);
        newButton = findViewById(R.id.new_stock_check);
        loadButton = findViewById(R.id.load_stock_check);
        foundText = findViewById(R.id.found_number);
        notFoundText = findViewById(R.id.not_found_number);

        LocationLists = new ArrayList<>();
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, LocationLists);
        startStopRFID.setText("START SCAN");
        refNumberText.setText("NONE");
        foundText.setText("0");
        notFoundText.setText("0");

        Global.eventStockCheckHandler = new EventHandler();

        /**
         * Refresh Button Listener
         */
        refreshButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ExecuteBackgroundTask(2, "");
            }
        });

        startStopRFID.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (startStopRFID.getText().equals("START SCAN")) {
                    if (Global.entryAdapter.list.size() > 1) {
                        if (BluetoothAdapter.getDefaultAdapter().isEnabled()) {
                            //open list of reader page
                            if (Global.connectedReader != null && Global.connectedReader.isConnected()) {
                                try {
                                    Global.connectedReader.Events.removeEventsListener(Global.eventKillRFIDHandler);
                                } catch (InvalidUsageException e) {
                                    e.printStackTrace();
                                } catch (OperationFailureException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    Global.connectedReader.Events.addEventsListener(Global.eventStockCheckHandler);
                                    Global.connectedReader.Actions.Inventory.perform();
                                    newButton.setEnabled(false) ;
                                    loadButton.setEnabled(false);
                                    updateStockCount.setEnabled(false);
                                    startStopRFID.setText("STOP SCAN");
                                } catch (InvalidUsageException e) {
                                    e.printStackTrace();
                                } catch (final OperationFailureException e) {
                                    e.printStackTrace();
                                    Toast.makeText(getBaseContext(), "Status Notification: " + e.getVendorMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                // list out readers.
                                openReadersPage(startStopRFID);
                            }
                        } else {
                            MessageBox msg = new MessageBox(EntryActivity.this, 0);
                            msg.showMessage_OK("Bluetooth is turned off. Please switch it on to use this feature.",startStopRFID);
                        }
                    } else {
                        MessageBox msg = new MessageBox(EntryActivity.this, 0);
                        msg.showMessage_OK("RFID tag list is empty.",startStopRFID);
                    }

                } else
                    try {
                        Global.connectedReader.Actions.Inventory.stop();
                        Global.connectedReader.Events.removeEventsListener(Global.eventStockCheckHandler);
                        newButton.setEnabled(true) ;
                        loadButton.setEnabled(true);
                        updateStockCount.setEnabled(true);
                        startStopRFID.setText("START SCAN");
                    } catch (InvalidUsageException e) {
                        e.printStackTrace();
                    } catch (OperationFailureException e) {
                        e.printStackTrace();
                    }
            }
        });

        /**
         * New Button Listener
         */
        newButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                newButton.setEnabled(false);
                    if (LocationSpinner.getSelectedItem().toString().equals("<Location>")) {
                        MessageBox msg = new MessageBox(EntryActivity.this, 0);
                        msg.showMessage_OK("Location is not chosen, please select a location before proceed.",newButton);
                    } else {
                        int row = RFIDTableHelper.numberOfRows("RFID");
                        SharedPreferences preferences = getSharedPreferences("StockCheck", MODE_PRIVATE);
                        String ref = preferences.getString("RefNo", "NONE");
                        if (row > 0 || ref.equals("NONE") == false) {
                            MessageBox msg = new MessageBox(EntryActivity.this, 1);
                            msg.showMessage_YesNo("Create new stock check will delete existing data, are you sure?",newButton);
                        } else {
                            ExecuteBackgroundTask(1, "");
                            newButton.setEnabled(true);
                        }
                    }

            }
        });

        loadButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                loadButton.setEnabled(false);
                String location = LocationSpinner.getSelectedItem().toString();
                if (location.equals("<Location>")) {
                    MessageBox msg = new MessageBox(EntryActivity.this, 0);
                    msg.showMessage_OK("Empty stock location, please select a location before proceed.",loadButton);
                } else {
                    int row = RFIDTableHelper.numberOfRows("RFID");
                    SharedPreferences preferences = getSharedPreferences("StockCheck", MODE_PRIVATE);
                    String ref = preferences.getString("RefNo", "NONE");
                    if (row > 0 || ref.equals("NONE") == false) {
                        MessageBox msg = new MessageBox(EntryActivity.this, 5);
                        msg.showMessage_YesNo("Loading stock check will delete existing data, are you sure?",loadButton);
                    } else {
                        openRecordsPage(loadButton);
                    }
                }

            }
        });

        updateStockCount.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                updateStockCount.setEnabled(false);
                    SharedPreferences preferences = getSharedPreferences("StockCheck", MODE_PRIVATE);
                    String ref = preferences.getString("RefNo", "NONE");

                    if (ref.equals("NONE")) {
                        MessageBox msg = new MessageBox(EntryActivity.this, 0);
                        msg.showMessage_OK("No existing record. Please perform stock check.",updateStockCount);
                    } else {
                        MessageBox msg = new MessageBox(EntryActivity.this, 4);
                        msg.showMessage_YesNo("Existing data will be removed once update successfully performed. Are you sure to proceed?",updateStockCount);
                    }
            }
        });

        /**
         * Add RFID List Header.
         */
        rfidListData.clear();
        rfidListData.add(new RFIDEntryData("**EPC ID**", "**Product Code**", "**Product Name**"));
        if(Global.entryAdapter == null)
            Global.entryAdapter = new RFIDAdapter(rfidListData);

        entryList.setAdapter(Global.entryAdapter);
        entryList.setLayoutManager(new LinearLayoutManager(this));

        SharedPreferences preferences = getSharedPreferences("StockCheck", MODE_PRIVATE);
        refNumberText.setText(preferences.getString("RefNo", "NONE"));

        foundText.setText(Integer.toString(RFIDTableHelper.getNumberOfFoundAndNotFound().getFound()));
        notFoundText.setText(Integer.toString(RFIDTableHelper.getNumberOfFoundAndNotFound().getNotFound()));

        /**
         * Get stock locations on first run.
         */
        ExecuteBackgroundTask(2, "");

        /**
         *  Load RFID data from DB if exists.
         */
        if (RFIDTableHelper.numberOfRows("RFID") > 0)
            ExecuteBackgroundTask(3, "");

    }

    /**
     * Background task executor.
     *
     * @param task 1=Retrieve RFID list | 2=Retrieve Stock Locations
     */
    public void ExecuteBackgroundTask(int task, String refNo) {
        switch (task) {
            case 1: {
                showProgress(true, entryFormView, mProgressView);
                rRFIDListTask = new RetrieveRFIDListTask(EntryActivity.this,false, refNo);
                rRFIDListTask.execute((Void) null);
                break;
            }
            case 2: {
                showProgress(true, entryFormView, mProgressView);
                rStockLocTask = new RetrieveStockLocationsTask(this);
                rStockLocTask.execute((Void) null);
                break;
            }
            case 3: {
                showProgress(true, entryFormView, mProgressView);
                lRFIDListTask = new LoadRFIDListTask(EntryActivity.this);
                lRFIDListTask.execute((Void) null);
                break;
            }
            case 4: {
                showProgress(true, entryFormView, mProgressView);
                updateStockCountTask = new UpdateStockCountTask(EntryActivity.this);
                updateStockCountTask.execute((Void) null);
                break;
            }
            case 5: {
                openRecordsPage(loadButton);
                break;
            }
            case 6: {
                showProgress(true, entryFormView, mProgressView);
                rRFIDListTask = new RetrieveRFIDListTask(EntryActivity.this,true, refNo);
                rRFIDListTask.execute((Void) null);
                break;
            }
            default:
                break;
        }
    }

    @Override
    public void handleTagResponse(String id, int found, int notFound) {
        if (entryList.getAdapter() == null) {
            entryList.setAdapter(Global.entryAdapter);
        }

        RFIDEntryData entryToBeRemoved = Global.entryAdapter.searchByTID(id);

        if (entryToBeRemoved != null) {
            Global.entryAdapter.remove(entryToBeRemoved);
            Global.entryAdapter.notifyDataSetChanged();
        }

        foundText.setText(Integer.toString(found));
        notFoundText.setText(Integer.toString(notFound));
    }

    /**
     * Background tasks for EntryActivity..
     */

    /**
     * Retrieve stock locations background task.
     */
    public class RetrieveStockLocationsTask extends AsyncTask<Void, Void, Boolean> {
        Context context;

        RetrieveStockLocationsTask(Context c) {
            this.context = c;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            SOAPWeb soapWeb = new SOAPWeb(baseContext);
            LocationLists = soapWeb.DownloadStockLocations();
            if (LocationLists.get(0).trim().equals("<Location>"))
                return true;
            else
                return false;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            rStockLocTask = null;
            showProgress(false, entryFormView, mProgressView);

            if (success) {
                adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, LocationLists);
                LocationSpinner.setAdapter(adapter);
            } else {
                Toast.makeText(getBaseContext(), LocationLists.get(0), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onCancelled() {
            rStockLocTask = null;
            showProgress(false, entryFormView, mProgressView);
        }
    }

    /**
     * Retrieve RFID list background task.
     */
    public class RetrieveRFIDListTask extends AsyncTask<Void, Void, Boolean> {
        Context context;
        String errorMsg = new String();
        boolean skipRecordCreation;
        String refNumber;

        RetrieveRFIDListTask(Context c, boolean skipRecordCreation, String refNumber) {
            this.context = c;
            this.skipRecordCreation = skipRecordCreation;
            this.refNumber = refNumber;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            String locationChosen;
            SOAPWeb.SOAPReturnRFIDDataList returnedData;
            SOAPWeb soapWeb = new SOAPWeb(baseContext);


                locationChosen = LocationSpinner.getSelectedItem().toString();
                locationChosen = locationChosen.substring(0, locationChosen.indexOf("-"));


            //rfidListData.add(new RFIDEntryData("**EPC ID**", "**Product Code**", "**Product Name**"));
            if(skipRecordCreation==false)
                returnedData = soapWeb.DownloadRFIDList(locationChosen, refNumber);
            else
                returnedData = soapWeb.DownloadRFIDList("", refNumber);

            if (!returnedData.getResult()) {
                errorMsg = returnedData.getMessage();
                return false;
            } else if (returnedData.getDataList().size() == 0) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        MessageBox msg = new MessageBox(EntryActivity.this, 0);
                        msg.showMessage_OK("No RFID tag is registered to the selected stock location.",null);
                    }
                });
            } else {
                if(!skipRecordCreation) {
                    refNumber = soapWeb.createStockCheckRecord(locationChosen);

                    if (refNumber == null) {
                        errorMsg = "Problem occur when creating record. Please ensure the location have inventory records.";
                        return false;
                    }
                }

                initializeSharedPrefAndRFIDList();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        refNumberText.setText(refNumber);

                    }
                });
                ArrayList<RFIDEntryData> data = returnedData.getDataList();
                SharedPreferences preferences = getSharedPreferences("StockCheck", MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                preferences = getSharedPreferences("StockCheck", MODE_PRIVATE);
                editor = preferences.edit();
                editor.putString("RefNo", refNumber);
                editor.putString("StkLoc", locationChosen);
                editor.commit();

                RFIDTableHelper.deleteAllData("RFID");

                RFIDTableHelper.insertDataIntoRFID(data);

                data.clear();
                data = RFIDTableHelper.getNotFoundRFIDTag();
                Global.entryAdapter.clear();
                Global.entryAdapter.insert(Global.entryAdapter.getItemCount(), new RFIDEntryData("**EPC ID**", "**Product Code**", "**Product Name**"));
                for (int i = 0; i < data.size(); i++) {
                    Global.entryAdapter.insert(Global.entryAdapter.getItemCount(), data.get(i));
                }
            }
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Global.entryAdapter.notifyDataSetChanged();
                    foundText.setText(Integer.toString(RFIDTableHelper.getNumberOfFoundAndNotFound().getFound()));
                    notFoundText.setText(Integer.toString(RFIDTableHelper.getNumberOfFoundAndNotFound().getNotFound()));
                }
            });

            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            rRFIDListTask = null;
            showProgress(false, entryFormView, mProgressView);

            if (success) {
                //
            } else {
                Toast.makeText(getBaseContext(), errorMsg, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public class LoadRFIDListTask extends AsyncTask<Void, Void, Boolean> {
        Context context;

        LoadRFIDListTask(Context c) {
            this.context = c;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {

            Global.entryAdapter.clear();
            Global.entryAdapter.insert(Global.entryAdapter.getItemCount(), new RFIDEntryData("**EPC ID**", "**Product Code**", "**Product Name**"));
            ArrayList<RFIDEntryData> data = new ArrayList<>();
            data.clear();
            data = RFIDTableHelper.getNotFoundRFIDTag();
            for (int i = 0; i < data.size(); i++) {
                Global.entryAdapter.insert(Global.entryAdapter.getItemCount(), data.get(i));
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Global.entryAdapter.notifyDataSetChanged();
                }
            });
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            lRFIDListTask = null;
            showProgress(false, entryFormView, mProgressView);

            if (success) {
                //
            } else {
                // Show message on error retrieve stock locations.
            }
            newButton.setEnabled(true);
        }
    }

    public class UpdateStockCountTask extends AsyncTask<Void, Void, Boolean> {
        Context context;
        SOAPWeb.SOAPReturn dataReturned;

        UpdateStockCountTask(Context c) {
            this.context = c;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            dataReturned = RFIDTableHelper.processStockCount();
            return dataReturned.getResult();
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            rStockLocTask = null;
            showProgress(false, entryFormView, mProgressView);

            if (success) {
                Toast.makeText(getBaseContext(), "Successfully updated to server.", Toast.LENGTH_SHORT).show();
                initializeSharedPrefAndRFIDList();
            } else {
                Toast.makeText(getBaseContext(), dataReturned.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onCancelled() {
            rStockLocTask = null;
            showProgress(false, entryFormView, mProgressView);
        }
    }

    /**
     * Message box for EntryActivity
     */
    public class MessageBox {

        private Context context;
        private int task;

        MessageBox(Context context, int task) {
            this.context = context;
            this.task = task;
        }

        public void showMessage_OK(String message, final Button button) {

            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Information");
            builder.setMessage(message);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    if(button!=null)
                        button.setEnabled(true);
                    // You don't have to do anything here if you just
                    // want it dismissed when clicked
                }
            });
            builder.setCancelable(false);

            // Create the AlertDialog object and return it
            AlertDialog alert = builder.create();
            alert.setCanceledOnTouchOutside(false);
            alert.show();
        }


        public void showMessage_YesNo(String message, final Button button) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);

            builder.setTitle("Confirm");
            builder.setMessage(message);

            builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    ExecuteBackgroundTask(task,"");
                    if (button != null)
                        button.setEnabled(true);

                }
            });

            builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    if (button != null)
                        button.setEnabled(true);
                }
            });
            builder.setCancelable(false);

            AlertDialog alert = builder.create();
            alert.setCanceledOnTouchOutside(false);
            alert.show();
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show, final View viewToHideShow, final View progressView) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            viewToHideShow.setVisibility(show ? View.GONE : View.VISIBLE);
            viewToHideShow.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    viewToHideShow.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            progressView.setVisibility(show ? View.VISIBLE : View.GONE);
            progressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            viewToHideShow.setVisibility(show ? View.GONE : View.VISIBLE);
            progressView.setVisibility(show ? View.VISIBLE : View.GONE);

        }
    }

    public void openReadersPage(View v) {
        // TableLayout settingsTable = (TableLayout) findViewById(R.id.settings_Table);

        Fragment myfragment;
        myfragment = new ReaderListFragment();
        ((ReaderListFragment) myfragment).callerActivity = "EntryActivity";
        Global.readerListFragment = myfragment;
        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.entryForm, myfragment);
        fragmentTransaction.commit();
    }

    public void openRecordsPage(View v)
    {
        Fragment myfragment;
        myfragment = new StockCheckRecordsListFragment();
        ((StockCheckRecordsListFragment) myfragment).setEnvironmentalVariables(LocationSpinner.getSelectedItem().toString(),baseContext,this);
        Global.recordListFragment = myfragment;
        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.entryForm, myfragment);
        fragmentTransaction.commit();
        loadButton.setEnabled(true);
    }

    public class EventHandler implements RfidEventsListener {

        @Override
        public void eventReadNotify(RfidReadEvents e) {

            final TagData[] myTags = Global.connectedReader.Actions.getReadTags(100);
            if (myTags != null) {
                for (int index = 0; index < myTags.length; index++) {
                    if (myTags[index].getOpCode() == ACCESS_OPERATION_CODE.ACCESS_OPERATION_READ &&
                            myTags[index].getOpStatus() == ACCESS_OPERATION_STATUS.ACCESS_SUCCESS) {
//                        if (myTags[index].getMemoryBankData().length() > 0) {
//                            System.out.println(" Mem Bank Data " + myTags[index].getMemoryBankData());
//                        }
                    }

                    if (myTags[index] != null && (myTags[index].getOpStatus() == null || myTags[index].getOpStatus() == ACCESS_OPERATION_STATUS.ACCESS_SUCCESS)) {
                        final int tag = index;
                        new ResponseHandlerTask(myTags[tag]).execute();
                    }

                }
            }
        }

        @Override
        public void eventStatusNotify(RfidStatusEvents rfidStatusEvents) {
            Toast.makeText(getBaseContext(), "Status Notification: " + rfidStatusEvents.StatusEventData.getStatusEventType(), Toast.LENGTH_SHORT).show();
        }
    }

    public class ResponseHandlerTask extends AsyncTask<Void, Void, Boolean> {
        private TagData tagData;
        private String epcInAscii;
        private int foundCount;
        private int notFoundCount;

        ResponseHandlerTask(TagData tagData) {
            this.tagData = tagData;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            boolean result = false;
            List<String> characters = new ArrayList<>();
            epcInAscii = new String();

            try {
                characters = Global.splitByLength(tagData.getTagID(), 2);
                epcInAscii = Global.convertHexToString(characters);
                RFIDTableHelper.updateStatusToFound(epcInAscii);

                DBHelper.FoundAndNotFoundRecordNumbers countResult = RFIDTableHelper.getNumberOfFoundAndNotFound();
                foundCount = countResult.getFound();
                notFoundCount = countResult.getNotFound();
                result = true;
            } catch (IndexOutOfBoundsException e) {
                //logAsMessage(TYPE_ERROR, TAG, e.getMessage());
                result = false;
            } catch (Exception e) {
                // logAsMessage(TYPE_ERROR, TAG, e.getMessage());
                result = false;
            }
            return result;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            cancel(true);
            handleTagResponse(epcInAscii,foundCount,notFoundCount);
            //if (oldObject != null && fragment instanceof ResponseHandlerInterfaces.ResponseTagHandler)
            //    ((ResponseHandlerInterfaces.ResponseTagHandler) fragment).handleTagResponse(epcInAscii);
            //oldObject = null;
        }
    }

    public void initializeSharedPrefAndRFIDList() {
        rfidListData.clear();
        SharedPreferences preferences = getSharedPreferences("StockCheck", MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("RefNo", "NONE");
        editor.putString("StkLoc", "");
        editor.commit();

        RFIDTableHelper.deleteAllData("RFID");
        RFIDTableHelper.deleteAllData("Balance");

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                refNumberText.setText("");
                foundText.setText("0");
                notFoundText.setText("0");
            }
        });
        Global.entryAdapter.clear();
        Global.entryAdapter.insert(Global.entryAdapter.list.size(),new RFIDEntryData("**EPC ID**", "**Product Code**", "**Product Name**"));
        Global.entryAdapter.notifyDataSetChanged();
    }

    @Override
    public void onRecordSelected(String refNo) {
        ExecuteBackgroundTask(6, refNo);
    }

    @Override
    public void onBackPressed() {
        if (startStopRFID.getText().equals("STOP SCAN")){
            AlertDialog.Builder builder = new AlertDialog.Builder(EntryActivity.this);

            builder.setTitle("Information");
            builder.setMessage("Reader is scanning, please stop the scan before exit.");

            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            builder.setCancelable(false);

            AlertDialog alert = builder.create();
            alert.setCanceledOnTouchOutside(false);
            alert.show();

        }else {
            super.onBackPressed();
        }
    }
}
