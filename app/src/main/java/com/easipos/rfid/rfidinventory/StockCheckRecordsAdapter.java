package com.easipos.rfid.rfidinventory;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Collections;
import java.util.List;

public class StockCheckRecordsAdapter extends RecyclerView.Adapter<StockCheckRecordViewHolder>{

    List<String> list = Collections.emptyList();
    Context context;
    CustomRecyclerViewClickListener listener;

    public StockCheckRecordsAdapter(List<String> list, CustomRecyclerViewClickListener listener) {
        this.list = list;
        this.listener = listener;
    }

    public StockCheckRecordViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.stock_check_record_row_layout, parent, false);
        final StockCheckRecordViewHolder holder = new StockCheckRecordViewHolder(v);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, holder.getAdapterPosition());
            }
        });
        return holder;

    }

    @Override
    public void onBindViewHolder(StockCheckRecordViewHolder holder, int position) {

        //Use the provided View Holder on the onCreateViewHolder method to populate the current row on the RecyclerView
        holder.refNo.setText(list.get(position));

        //animate(holder);
    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return list.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    // Insert a new item to the RecyclerView on a predefined position
    public void insert(int position, String data) {
        list.add(position, data);
        //notifyItemInserted(position);
    }

    // Remove a RecyclerView item containing a specified Data object
    public void remove(String data) {

        int position = list.indexOf(data);
        list.remove(position);
        notifyItemRemoved(position);
    }
}
